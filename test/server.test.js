const assert = require("assert");

describe("server", function() {
  const logger = false;
  const server = require("../src/server");

  describe("when the request takes too long", () => {
    const git = {
      commits() {
        // "Long" lasting git action
        return new Promise((resolve) => setTimeout(() => resolve([]), 30));
      },
    };
    const maxTime = 10;

    it("should return 408 Reques Timeup", async function() {
      const { api } = await server({ git, maxTime, logger });

      const res = await api.inject({
        method: "GET",
        url: "/?url=foo",
      });

      assert.equal(res.statusCode, 408);
    });
  });

  describe("when the request does not have an url", () => {
    const git = {
      commits() {
        return Promise.resolve([]);
      },
    };

    it("should return 400 Bad Request", async function() {
      const { api } = await server({ git, logger });

      const res = await api.inject({
        method: "GET",
        url: "/",
      });

      assert.equal(res.statusCode, 400);
    });
  });

  describe("when the request has a valid git url", () => {
    const commits = [
      {
        sha: "abcde2",
        author: "Jane Doe",
        email: "jane.doe@example.com",
        date: "2018-01-01 02:00 +0100",
        subject: "commit #2",
        body: "commit body",
      },
      {
        sha: "abcde1",
        author: "Jane Doe",
        email: "jane.doe@example.com",
        date: "2018-01-01 01:00 +0100",
        subject: "commit #1",
        body: "commit body",
      },
    ];
    const git = {
      commits() {
        return Promise.resolve(commits);
      },
    };

    const pagination = {
      currentPage: 1,
    };

    it("should return the list of commits", async function() {
      const { api } = await server({ git, logger });

      const res = await api.inject({
        method: "GET",
        url: "/?url=http://github.com/jhbabon/scout",
      });

      assert.equal(res.statusCode, 200);
      assert.deepEqual(JSON.parse(res.body), { commits, pagination });
    });
  });
});
