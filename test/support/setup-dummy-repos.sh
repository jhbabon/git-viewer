#!/usr/bin/env bash

echo "===> Preparing dummy repositories"
current=$(pwd)

echo "---> Preparing repo in good state"
mkdir -p tmp/dummy-repos/ok/
cd tmp/dummy-repos/ok
git init .
git config --add user.name "Jane Doe"
git config --add user.email "jane.doe@example.com"

for (( i = 1; i <= 15; i++ )); do
  touch "$i.txt"
  git add "$i.txt"
  git commit -m "commit #$i" -m "commit body"
done

cd "$current"

echo "---> Preparing repo in bad state"
mkdir -p tmp/dummy-repos/bad/
cd tmp/dummy-repos/bad
# git-log will fail in a repository without any commit in it
git init .
