const path = require("path");
const fs = require("fs");
const assert = require("assert");

describe("git.shell", function() {
  const shell = require("../../src/git/shell");
  const repos = path.join(__dirname, "..", "..", "tmp", "dummy-repos");

  before(function(done) {
    fs.access(repos, function(error) {
      if (error) {
        const { exec } = require("child_process");
        const cwd = path.join(__dirname, "..", "..");
        exec("test/support/setup-dummy-repos.sh", { cwd }, function(error) {
          if (error) {
            return done(error);
          }

          done();
        });
      } else {
        done();
      }
    });
  });

  describe("#commits()", function() {
    describe("when the repository is in good state", function() {
      const repo = path.join(repos, "ok");

      const assertCommit = function(commit, index, commits, offset = 0) {
        assert(/^[a-z|\d]{40}$/.test(commit.sha));
        assert.equal("Jane Doe", commit.author);
        assert.equal("jane.doe@example.com", commit.email);
        assert(
          /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \+\d{1,4}$/.test(commit.date),
        );

        const id = commits.length - index + offset;
        assert.equal(`commit #${id}\n\ncommit body\n`, commit.message);
      };

      it("should return the first 10 commits", async function() {
        const commits = await shell.commits({ repo });

        assert.equal(commits.length, 10);

        commits.forEach(function(commit, index) {
          assertCommit(commit, index, commits, 5);
        });
      });

      it("should return the first next page of commits", async function() {
        const commits = await shell.commits({ repo, page: 2 });

        assert.equal(commits.length, 5);

        commits.forEach(assertCommit);
      });

      it("should return empty list if there is no more pages", async function() {
        const commits = await shell.commits({ repo, page: 3 });

        assert.equal(commits.length, 0);
      });
    });

    describe("when the repository is in bad state", function() {
      const repo = path.join(repos, "bad");

      it("should return an error", async function() {
        try {
          await shell.commits({ repo });
        } catch (error) {
          return assert(true);
        }

        assert(false, "No exception thrown");
      });
    });

    describe("when the repository does not exist", function() {
      const repo = path.join(repos, "404");

      it("should return an error", async function() {
        try {
          await shell.commits({ repo });
        } catch (error) {
          return assert(true);
        }

        assert(false, "No exception thrown");
      });
    });
  });
});
