const path = require("path");
const fs = require("fs");
const assert = require("assert");
const nock = require("nock");

describe("git.hub", function() {
  const hub = require("../../src/git/hub");
  const repo = "https://github.com/jhbabon/dotfiles";

  describe("when the request to GitHub fails", function() {
    before(function() {
      nock("https://api.github.com")
        .get("/repos/jhbabon/dotfiles/commits")
        .query({ page: 1, per_page: 10 })
        .reply(404, "ops");
    });

    it("should return an error", async function() {
      try {
        await hub.commits({ repo });
      } catch (error) {
        return assert(true);
      }

      assert(false, "No exception thrown");
    });
  });

  describe("when the request to GitHub is successful", function() {
    describe("when requesting the first page", function() {
      before(function() {
        const fixturePath = path.join(
          __dirname,
          "..",
          "fixtures",
          "github",
          "commits-ok-page-1.json",
        );
        const fixture = fs.readFileSync(fixturePath);

        nock("https://api.github.com")
          .get("/repos/jhbabon/dotfiles/commits")
          .query({ page: 1, per_page: 10 })
          .reply(200, fixture);
      });

      it("should return the list of commits", async function() {
        const expectedPath = path.join(
          __dirname,
          "..",
          "fixtures",
          "viewer",
          "commits-ok-page-1.json",
        );
        let expected = fs.readFileSync(expectedPath);
        expected = JSON.parse(expected);

        const commits = await hub.commits({ repo });

        assert.deepEqual(commits, expected.commits);
      });
    });

    describe("when requesting the second page", function() {
      before(function() {
        const fixturePath = path.join(
          __dirname,
          "..",
          "fixtures",
          "github",
          "commits-ok-page-2.json",
        );
        const fixture = fs.readFileSync(fixturePath);

        nock("https://api.github.com")
          .get("/repos/jhbabon/dotfiles/commits")
          .query({ page: 2, per_page: 10 })
          .reply(200, fixture);
      });

      it("should return the list of commits", async function() {
        const expectedPath = path.join(
          __dirname,
          "..",
          "fixtures",
          "viewer",
          "commits-ok-page-2.json",
        );
        let expected = fs.readFileSync(expectedPath);
        expected = JSON.parse(expected);

        const commits = await hub.commits({ repo, page: 2 });

        assert.deepEqual(commits, expected.commits);
      });
    });
  });
});
