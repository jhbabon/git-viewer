const prettierrc = {
  trailingComma: "all",
  arrowParens: "always",
};

module.exports = prettierrc;
