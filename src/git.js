/**
 * Main git module. It hides implementation details (how to get commits)
 * and exposes only the functions to get commit information.
 */

const hub = require("./git/hub");
const shell = require("./git/shell");

// TODO: Better error handling.
// The main problem now is that errors are leaking information
// (e.g: if the git shell command fails).
// Throwing custom errors when necessary would be way better.
const commits = async (options) => {
  try {
    // We try first GitHub API
    return await hub.commits(options);
  } catch (_) {
    // If it fails, we try the shell
    return await shell.commits(options);
  }
};

const git = { commits };

module.exports = git;
