const server = require("./server");
const git = require("./git");

const main = async () => {
  const port = process.env.PORT || 3000;
  const app = await server({ git, port });
  app.run();

  return app;
};

main().catch((error) => {
  console.error(error);
  process.exit(1);
});
