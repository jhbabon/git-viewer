/**
 * Main module to use git in the current shell to extract commits
 * information.
 */

const util = require("util");
const exec = util.promisify(require("child_process").exec);
const mkdtemp = util.promisify(require("fs").mkdtemp);
const os = require("os");
const path = require("path");

const convert = require("xml-js");
const del = require("del");

/**
 * We are going to use git-log to get all the commits in a repo, but we
 * need to parse the git-log output. git-log allows custom formats
 * so we can use a structured format to extract all the information
 * we want from the log output.
 *
 * XML is a format that has the features we need to parse text:
 *  - It's structured
 *  - It can deal with any kind of text by using CDATA
 *
 * Given this, we can use a simple XML structure for the log and the
 * parse it in a deterministic way.
 *
 * We want each commit to look like this:
 *
 *   <commit>
 *     <sha>43513df72fceaa4dd07b1e1ac1ebe8a192ea0b83</sha>
 *     <author>Jane Doe</author>
 *     <email>jane.doe@example.com</email>
 *     <date>2018-01-01 01:00 +0</date>
 *     <message><![CDATA[Change indentation\nFollow linter standars]></message>
 *   </commit>
 *
 * This format variable declares the main XML format with the flags
 * that git-log needs in order to know where to put the information
 */
const format = [
  "<commit>",
  "<sha>%H</sha>",
  "<author>%an</author>",
  "<email>%ae</email>",
  "<date>%ai</date>",
  "<message><![CDATA[%B]]></message>",
  "</commit>",
].join("");

/**
 * Transform a xmljs commit object into a simpler
 * JS object.
 *
 * @example
 *   const xmljs = { sha: { _text: "abdce" } };
 *   const commit = transform(xmljs);
 *   console.log(commit);
 *   //=> { sha: "abdce" }
 *
 * @param {Object} xmljs - The object parsed by xml-js
 * @return {Object} - The Commit object where every XML tag
 *   is an attribute and the value of the XML tag (text or cdata)
 *   is the value of the JS object attribute.
 */
const transform = (xmljs) => {
  const reducer = (commit, [key, value]) => {
    if (value._text) {
      commit[key] = value._text;
    } else if (value._cdata) {
      commit[key] = value._cdata;
    }

    return commit;
  };

  return Object.entries(xmljs).reduce(reducer, {});
};

/**
 * Transform the output returned by git-log into JS objects
 *
 * The output returned by git-log will be a colleciton of XML
 * tags. @see format.
 *
 * We can delegate the heavy work of extracting info
 * to a proper XML library and then transform that information
 * into our own JS Objects.
 *
 * @example
 *   const output = "<commit><sha>abcd</sha></commit><commit><sha>123</sha></commit>";
 *   const commits = parse(output);
 *   console.log(commits);
 *  //=> [ { sha: "abcd" }, { sha: "123" } ]
 *
 * @param {Buffer|string} data - git-log output with XML format
 * @return {Array<Object>}
 */
const parse = (data) => {
  const xml = `<root>${data}</root>`;
  const { root } = convert.xml2js(xml, { compact: true });

  if (!root.commit) {
    return [];
  }

  if (Array.isArray(root.commit)) {
    return root.commit.map(transform);
  } else {
    return [transform(root.commit)];
  }
};

/**
 * Generate the temporal dir, or sandbox, where the git repo will live
 *
 * @return {string} - The path to the git repository
 */
const sandbox = () => {
  const tmpDir = path.join(os.tmpdir(), "git-viewer-");

  return mkdtemp(tmpDir);
};

/**
 * Clone the repository in the cwd (current working dir)
 *
 * @param {Object} options
 * @param {string} options.repo - Repo URL
 * @param {string} options.cwd - Current Working Dir
 *
 * @return {Promise}
 */
const clone = ({ repo, cwd }) => {
  return exec(`git clone --quiet --bare ${repo} .`, { cwd });
};

/**
 * Get the log of all the commits in the git repository.
 *
 * The log will have all the commits info in XML format. @see format.
 *
 * @param {Object} options
 * @param {string} options.repo - Repo URL
 * @param {string} options.cwd - Current Working Dir
 *
 * @return {Buffer}
 */
const log = async ({ repo, cwd, page = 1 }) => {
  const number = 10;
  const skip = (page - 1) * number;
  const { stdout } = await exec(
    `git log -n ${number} --skip=${skip} --pretty=format:"${format}"`,
    { cwd },
  );

  return stdout || Buffer.alloc(0);
};

/**
 * Remove the temporal sandbox after getting the information
 *
 * @param {string} cwd - path to the dir to remove
 */
const cleanup = async (cwd) => {
  try {
    await del([cwd], { force: true });
  } catch (_) {
    // it's fine not to return the error when cleaning up,
    // this is just a side effect and it shouldn't stop
    // the main logic
  }
};

/**
 * Given a git repository URL, return a list of all the commits.
 *
 * @param {Object} options
 * @param {string} options.repo - Repo URL
 *
 * @return {Promise<Array<Object>>}
 */
const commits = async ({ repo, page = 1 }) => {
  const cwd = await sandbox();
  await clone({ repo, cwd });
  const list = await log({ repo, cwd, page });

  await cleanup(cwd);

  return parse(list);
};

/**
 * Main shell module. It exposes only the #commits() function
 */
const shell = { commits };

module.exports = shell;
