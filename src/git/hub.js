/**
 * Main module to use GitHub API to extract commits information
 */

const got = require("got");
const { URL } = require("url");

const base = "https://api.github.com";

/**
 * Move GitHub commit info into our own format
 *
 * @param {Object} item. @see https://developer.github.com/v3/repos/commits/
 *
 * @return {Object}
 */
const transform = ({ sha, commit }) => {
  return {
    sha,
    author: commit.author.name,
    email: commit.author.email,
    date: commit.author.date,
    message: commit.message,
  };
};

/**
 * Get the GitHub API URL from the given repo URL and the page
 *
 * The repo URL is expected in the format:
 *   https://github.com/:user/:repo
 *
 * @param {Object} options
 * @param {string} options.repo
 * @param {integer} [options.page] - It defaults to 1.
 *
 * @return {string} - GitHub API endpoint
 */
const apiURL = ({ repo, page = 1 }) => {
  const url = new URL(repo);

  return `${base}/repos${url.pathname}/commits?page=${page}&per_page=10`;
};

/**
 * Fetch all the commits from the given GitHub repository
 *
 * @param {Object} options
 * @param {string} options.repo
 * @param {integer} [options.page] - It defaults to 1.
 *
 * @return {{Promise<Array<Object>>}
 */
const commits = async (options) => {
  const response = await got.get(apiURL(options), { json: true });

  return response.body.map(transform);
};

const hub = { commits };

module.exports = hub;
