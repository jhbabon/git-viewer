const fastify = require("fastify");

const MAX_TIME = 1000 * 60 * 2; // 2 min

/**
 * Guard against too long requests
 *
 * @param {integer} maxTime - Max time to wait, in milliseconds
 *
 * @return {Function} - fastify hook function
 */
const timeGuard = (maxTime) => {
  return (request, reply, next) => {
    let timer;
    const timeout = () => {
      timer = null; // prevent memory leaks
      if (!reply.sent) {
        reply.code(408).send({ error: "Request Timeout" });
      }
    };

    // TODO: This timeout control prevents the client to wait, but
    // it doesn't stop any long shell command or API call to GitHub.
    // That could be a problem because the app could consume too many
    // resources and be unresponsive. It would be better to, whenever
    // there is a timeout, close any running process or API call.
    timer = setTimeout(timeout, maxTime);

    next();
  };
};

/**
 * Fastify schema for the URL requests to get the list of commits
 */
const schema = {
  querystring: {
    type: "object",
    properties: {
      url: { type: "string" },
      page: { type: "integer" },
    },
    required: ["url"],
  },
};

/**
 * Main route action to get the list of commits from a repo url.
 *
 * @param {Object} git - The git module. @see git
 *
 * @return {Function} - Fastify route function
 */
const index = (git) => {
  return async (request, reply) => {
    const repo = request.query.url;
    const page = request.query.page || 1;
    const pagination = {
      currentPage: page,
    };
    const commits = await git.commits({ repo, page });

    // TODO: Probably the pagination information should be
    // managed by each git engine (GitHub or Shell).
    if (commits.length === 10) {
      pagination["nextPage"] = page + 1;
    }

    reply.type("application/json; charset=utf-8");
    reply.send({ commits, pagination });
  };
};

/**
 * Main server function.
 *
 * This function will declare the HTTP rules and it will return
 * an object to start to listen to incoming requests
 *
 * @param {Object} options
 * @param {Object} options.git - Git module to use.
 *   It must respond to #commits(). @see git
 * @param {boolean} [options.logger] - Whether to use the default
 *   logger or not. Defaults to true
 * @param {intenger} [options.port] - Port to listen for HTTP requests
 * @param {integer} options.maxTime - Max timeup time. Defaults to MAX_TIME
 *
 * @return {Object} - With the api (fastify), and a #run() function
 */
const server = async ({
  git,
  logger = true,
  port = 3000,
  maxTime = MAX_TIME,
}) => {
  const api = fastify({ logger });
  api.addHook("preHandler", timeGuard(maxTime));
  api.get("/", { schema }, index(git));

  const run = () => {
    api.listen(port, (err, address) => {
      if (err) {
        throw error;
      }
      api.log.info(`server listening on ${address}`);
    });
  };

  const close = () => api.close();

  return { api, run, close };
};

module.exports = server;
