# Git Viewer

Small `node.js` app that lists all the commits in a GitHub repository.

## Installation

This app depends on `node.js` `v8.12.0`. You can download `node.js` from the main [website][nodejs].

You can also use a tool like [nodenv][nodenv] to install different versions of `node.js`.

Once you have `node.js` in place, you will need to install all the dependencies using `npm`:

```
$ npm install --production
```

## Usage

This is a small web app, which means that you need to start it and make requests against it.

Once all the dependencies have been installed, you can start the app with `node`:

```
$ node src/main.js
{"level":30,"time":1537734014361,"msg":"Server listening at http://127.0.0.1:3000","pid":11649,"hostname":"discovery","v":1}
{"level":30,"time":1537734014362,"msg":"server listening on http://127.0.0.1:3000","pid":11649,"hostname":"discovery","v":1}
```

In another shell, you can make HTTP requests. For example, using curl:

```
$ curl "http://localhost:3000?url=https://github.com/jhbabon/scout" | python -m json.tool
{
    "commits": [
        {
            "sha": "aa01677aa3834dc5c22a1e55bcbb9e4c7c32d199",
            "author": "Juan Hern\u00e1ndez",
            "email": "juan.hernandez.babon@gmail.com",
            "date": "2018-01-26 16:00:12 +0000",
            "message": "Add info about how to use homebrew to install scout"
        },
        {
            "sha": "bfd6f3fcdc5b7a08941a39b9049a12a0dadeb6da",
            "author": "Juan Hern\u00e1ndez",
            "email": "juan.hernandez.babon@gmail.com",
            "date": "2018-01-14 19:56:50 +0000",
            "message": "Bump v1.3.0"
        },
        //...
    ],
    "pagination": {
        "currentPage": 1,
        "nextPage": 2
    }
}
```

## Development

To use development tools you will need to install all the dependencies:

```
$ npm install
```

Now you can start the server using [nodemon][nodemon]. It will restart the server if there are changes in the code.

```
$ npx nodemon
```

There is a linter to fix the code formatting:

```
$ npm run lint
$ npm run lint:fix # to fix issues
```

## Tests

To run the tests you will need to install all the dependencies:

```
$ npm install
```

Then you can run the test command:

```
$ npm test
```

[nodejs]: https://nodejs.org/
[nodenv]: https://github.com/nodenv/nodenv
[nodemon]: https://nodemon.io/
